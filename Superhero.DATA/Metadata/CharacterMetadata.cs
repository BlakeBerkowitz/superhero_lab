﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Superhero.DATA
{

    [MetadataType(typeof(CharacterMetadata))]

    public partial class character { }

    class CharacterMetadata
    {
        public int CharacterID { get; set; }

        [Required(ErrorMessage ="* Required")]
        [Display(Name="name")]
        public string name { get; set; }

        [Display(Name = "alias")]
        public string alias { get; set; }

        [Display(Name = "origin")]
        public string origin { get; set; }
        [Display(Name = "alignment")]
        [Required(ErrorMessage = "* Required")]
        public string alignment { get; set; }



    }
}
