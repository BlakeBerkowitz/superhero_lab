//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Superhero.DATA
{
    using System;
    using System.Collections.Generic;
    
    public partial class Character
    {
        public int CharacterID { get; set; }
        public string name { get; set; }
        public string alias { get; set; }
        public string origin { get; set; }
        public string alignment { get; set; }
    }
}
